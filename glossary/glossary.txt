灵水	spiritual water
游小默	You XiaoMo
灵水	spiritual water
丹	pill
灵草	magic herb
乔无星	Qiao WuXing
天心	Tian Xin
逍遥院	XiaoYao Academy
颜辉	Yan Hui
程向荣	Cheng XiangRong
驭兽	Beast Transfiguration
无星	WuXing
无双	WuShuang
蛇球	SheQiu
熊笑	Xiong Xiao
凌霄	Ling Xiao
吞金兽	Metal Swallowing Beast
独眼刀疤	One-Eyed Scarface
小黑	XiaoHei
灵魂之力	soul force
灵气	spiritual energy
丹师	Mage
皮球	PiQiu
毛球	MaoQiu
猫球	CatQiu
蛇球	SheQiu
玉简	jade drive
游安泰	You AnTai
段奇天	Duan QiTian
于步	Yu Bu
通天大陆	TongTian Continent
安桥	An Qiao
空间符	teleportation talisman
灵晶	spirit gem
丹师工会	Mage Association
驭兽工会	Beast Transfiguration Guild/Association
闭关	secluded cultivation 
本命契约	lifebound contract
灵晶	Spirit gems
晋级	advance
识海	sea of consciousness
人境	Man Realm
地境	Earth Realm
天境	Sky Realm
日境	Sun Realm
月境	Moon Realm
星境	Star Realm
辰境	Celestial Realm
灵境	Spiritual Realm
皇境	Imperial Realm
帝境	Emperor Realm
神境	Divine Realm
圣境	Sacred Realm
妖凰族	Demon Phoenix Clan
姬凤	Ji Feng
麒麟族	Qilin Clan
太白	Tai Bai
苍盟盟主	Head of the Cang Alliance
方迟尧	Fang ChiYao
修炼者	practitioner
师侄	Martial Niece
臭老头	Shitty old geezer
老家伙	Old fart
强者	expert
秦大哥	Big Brother Qin
游兄弟	Fellow/Fellow Brother You
灵气	spiritual energy
灵草	magic herb
炼丹	Refine pills/magic pills
灵魂之力	soul power/ power of the soul
修炼者	practitioner
淬炼	Distill herb/magic herb
丹师	Mage
功法	Soul training
技法	Skill training
巩固	consolidate
境界 	realm
灵魂技法	Skill training manual
灵水	spiritual water
中阶下品的灵魂技法	low-grade middle level skill manual
六级上品丹师 	high-grade level six mage
高阶中品功法 	high level 'mid-grade soul training manual'
低阶上品技法 	high-grade low level skill training manual
高阶上品 	high-grade high level manual
几株	a few stalks
六十副	60 sets
储物袋里	magic bag
天魂经	Heavenly Soul Scripture
传送阵	Transport circle
玉简	Jade drive
水晶石 	crystal orbs
金冥鼎 	Jin Ming Cauldron
图集	encyclopedia
清心丹	mental purification pill
黄沙丹	Golden Sands pill
天灵丹 	TianLing pill 
固元丹 	GuYuan pills
生骨丹	Bone Growth Pill
活血丹	Blood Circulation Pill
容颜丹	Beauty Pill
凤凰丹 	Phoenix Pill
还命丹 	Revival Pill
玉髓丹	Chalcedony Pill 
乙级学区	Ward B
三班	Class Three
丹师公会	Mage Association
一区	Block 1
二区	Block 2
学区	Ward
销售点	Sales area
黑印村	Hei Yin Village
天堂境	Paradise Realm/ Heavenly Paradise
技阁	Skills Pavilion
功法阁	Soul Training Pavilion 
技法阁	Skill Training Pavilion
赛区	competition zone/venue/division
修为不高	level/cultivation base  was not that high
默默地抹掉一脸血	silently wiped the blood off his face
超友谊	super close
小黄瓜	balls testicles
自作多情	showering affection on an uninterested party
心中的想法	Inner thoughts
炙手可热	hot topic
面色不善	eyed him with distaste
xx 家	xx Family/Clan
五大势力	Five Great Powers
刀门	Dao sect
炎帮	Yan group
四大家族	Four Big Clans
学院八大巨头	DaoXin Academy's Eight Giants
点数	Point
等級	level
晉級 	gained a level
能量罩笼	Energy shield
淡淡的能量薄膜	thin layer of energy film 
晶点	Luminous fragments
步骤	Process/steps
融合	Refining
百人榜	Top hundred ranks/rankings
纠缠不清	pester/harass/hound
晋级	advance
识海	sea of consciousness
高级位面	Higher Level Realm
中级位面	Middle Level Realm
帝王兽	Emperor Beast
无尽之海	Boundless Sea
灵脉	leyline
化形草	Shapeshifting Herb
元素之心	Elemental Essence
血脉	bloodline
本命契约	lifebound contract
灵晶	Spirit gems
琉璃水蛟	Azure Sea Dragon
闭关	secluded cultivation 
甲级学区	Ward A
破风声	whistling through the air
本命火焰	Lifebound Natal Flame
精血 	Life Blood
空间传送	Dimensional Teleportation
苍盟	Cang Alliance
四灵帝王兽	Four Divine Emperor Beasts
赤血家族	Vermilion Blood Clan
游震天	You ZhenTian
圣境	Sacred level
银戈	Yin Ge
展雨轩	Zhan YuXuan
风迟云	Feng ChiYun
彩级六品	Grade Six Rainbow Level
夏音	Xia Yin
莫玛	Mo Ma
谢俊	Xie Jun
白虎宫殿	White Tiger Palace
五大灵兽	Five Great Spiritual Beasts
妖兽	demon beast
金翅虫	Golden Winged Insect
